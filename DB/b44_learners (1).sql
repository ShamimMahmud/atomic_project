-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2017 at 04:40 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `b44_learners`
--

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(115) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(115) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `soft_delete`) VALUES
(1, 'Shamim', 'male', 'Yes'),
(2, 'shakil', 'male', 'Yes'),
(3, 'Runa', 'female', 'Yes'),
(4, 'Asad', 'male', 'Yes'),
(5, 'Asif', 'female', 'Yes'),
(6, 'Asaduzzaman Babu', 'male', 'No'),
(8, 'Saiful', 'male', 'Yes'),
(9, 'Sarwar Hossain', 'male', 'No'),
(10, 'Sadia Sultana', 'female', 'No'),
(11, 'Ashfaq', 'female', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `name` varchar(115) COLLATE utf8_unicode_ci NOT NULL,
  `hobby` varchar(115) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobby`, `soft_delete`) VALUES
(10, 'Shamim Mahmud', 'Gardening,Reading books', 'No'),
(11, 'Shakil Mahmud', 'Gardening,Music,Facebooking', 'No'),
(12, 'Asaduzzaman Babu', 'Gardening,Music', 'Yes'),
(13, 'Hasanuzzaman ', 'Reading books', 'Yes'),
(14, 'Amir', 'Gardening,Music,Facebooking', 'Yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
