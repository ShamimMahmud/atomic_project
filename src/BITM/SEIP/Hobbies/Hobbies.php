<?php
/**
 * Created by PhpStorm.
 * User: Shamim
 * Date: 01/02/2017
 * Time: 02:01 AM
 */

namespace App\Hobbies;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

use PDO;
use PDOException;


class Hobbies extends DB
{

    private $id;
    private $name;
    private $hobby;

    public function setData($allPostData){

        if(array_key_exists("id",$allPostData)) {

            $this->id=$allPostData['id'];
        }

        if(array_key_exists("name",$allPostData)) {

            $this->name=$allPostData['name'];
        }


            if (array_key_exists("hobby", $allPostData)) {

                //$this->hobby = $allPostData["hobby"];
                $str = implode(",",$allPostData["hobby"]);
                $this->hobby=$str;



            }
    }
    public function store(){
        $arrData  =  array($this->name,$this->hobby);
        
        if(!empty($this->name) && !empty($this->hobby)) {

            $query = 'INSERT INTO hobbies (name,hobby) VALUES (?,?)';

            $STH = $this->DBH->prepare($query);
            $result = $STH->execute($arrData);

            if ($result) {
                Message::setMessage("Success! Data has been stored successfully!");
            } else {
                Message::setMessage("Failed! Data has not been stored!");
            }
            
        }

        else {
            Message::setMessage("Please, fill all the fields!!!");
        }

        Utility::redirect('index.php');

    }

    public function index(){

        $sql = "select * from hobbies where soft_delete='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(\PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from hobbies where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(\PDO::FETCH_OBJ);

        return $STH->fetch();

    }
    public function trashed(){

        $sql = "select * from hobbies where soft_delete='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(\PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function update(){

        $arrData = array($this->name,$this->hobby);

        $sql = "UPDATE  hobbies SET name=?,hobby=? WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        Utility::redirect('index.php');


    }



    public function trash(){

        $sql = "UPDATE  hobbies SET soft_delete='Yes' WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('index.php');


    }


    public function recover(){

        $sql = "UPDATE  hobbies SET soft_delete='No' WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Recovered  :( ");


        Utility::redirect('index.php');


    }




    public function delete(){

        $sql = "Delete from hobbies  WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");


        Utility::redirect('index.php');


    }


    public function indexPaginator($page=1,$itemsPerPage=3){
        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from hobbies  WHERE soft_delete = 'No' LIMIT $start,$itemsPerPage";



        }catch (PDOException $error){

            $sql = "SELECT * from hobbies  WHERE soft_delete = 'No'";

        }

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }



    public function trashedPaginator($page=1,$itemsPerPage=3){

        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from hobbies  WHERE soft_delete = 'Yes' LIMIT $start,$itemsPerPage";



        }catch (PDOException $error){

            $sql = "SELECT * from hobbies  WHERE soft_delete = 'Yes'";

        }

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;




    }






    public function trashMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id){

            $sql = "UPDATE  hobbies SET soft_delete='Yes' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('trashed.php?Page=1');


    }


    public function recoverMultiple($markArray){


        foreach($markArray as $id){

            $sql = "UPDATE  hobbies SET soft_delete='No' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");


        Utility::redirect('index.php?Page=1');


    }



    public function deleteMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id){

            $sql = "Delete from hobbies  WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");


        Utility::redirect('index.php?Page=1');


    }



    public function listSelectedData($selectedIDs){



        foreach($selectedIDs as $id){

            $sql = "Select * from hobbies  WHERE id=".$id;


            $STH = $this->DBH->query($sql);

            $STH->setFetchMode(PDO::FETCH_OBJ);

            $someData[]  = $STH->fetch();


        }


        return $someData;


    }




    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byHobby']) )  $sql = "SELECT * FROM `hobbies` WHERE `soft_delete` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `hobby` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byHobby']) ) $sql = "SELECT * FROM `hobbies` WHERE `soft_delete` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byHobby']) )  $sql = "SELECT * FROM `hobbies` WHERE `soft_delete` ='No' AND `hobby` LIKE '%".$requestArray['search']."%'";

        if( isset($requestArray['byname']) && isset($requestArray['byhobby']) )  $sql = "SELECT * FROM `hobbies` WHERE `soft_delete` ='Yes' AND (`name` LIKE '%".$requestArray['search']."%' OR `hobby` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byname']) && !isset($requestArray['byhobby']) ) $sql = "SELECT * FROM `hobbies` WHERE `soft_delete` ='Yes' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byname']) && isset($requestArray['byhobby']) )  $sql = "SELECT * FROM `hobbies` WHERE `soft_delete` ='Yes' AND `hobby` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()




    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->hobby);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->hobby);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords

    public function getAllKeys()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->trashed();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }

        $allData = $this->trashed();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->trashed();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->hobby);
        }
        $allData = $this->trashed();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->hobby);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords



}