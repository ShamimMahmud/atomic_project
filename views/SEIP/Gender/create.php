

<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender Create Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <style>
        .center_div{
            margin: 0 auto;
            width:80% /* value of choice which suits alignment */
        }
    </style>

</head>
<body>

<div class="container">

    <div class="navbar">

        <center><td><a href='index.php' class='btn btn-group-lg alert-success'>View Active-List</a>
                <a href="trashed.php"   class="btn btn-group-lg alert-warning role="button"> View Trashed List</a>
            </td></center>

    </div>
</div>

<div class="panel container center_div">
    <div class="panel-heading" style="background-color:#8aa6c1;color: #ffffff">
        <h1 class="display-1 text-center">Add Gender</h1>
    </div>

    <div class="panel-body">
    <form  class="form-group" action="store.php" method="post">

        Enter Name:
        <input class="form-control" type="text" name="name">
        <br>
            Choose Your Gender:<br><br>

                <input type="radio" name="gender" value="male"> Male<br>

                <input type="radio" name="gender" value="female"> Female<br><br>
            <button type="submit" class="btn bg-success btn-lg btn-block">Submit</button>

    </form>

</div>
</div>



<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


