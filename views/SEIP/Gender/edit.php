<?php
require_once("../../../vendor/autoload.php");


use App\Message\Message;
use App\Gender\Gender;


  if(!isset($_SESSION)){
      session_start();
  }
  $msg = Message::getMessage();

  echo "<div id='message'> $msg </div>";


$objGender = new Gender();
$objGender->setData($_GET);
$oneData = $objGender->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender Edit Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <style>
        .center_div{
            margin: 0 auto;
            width:80% /* value of your choice which suits your alignment */
        }
    </style>

</head>
<body>
<br><br>

<div class="panel container center_div">
    <div class="panel-heading" style="background-color:#1b6d85;color: #ffffff">
        <h1 class="display-1 text-center">Edit Information</h1>
    </div>
    <div class="container panel-body center_div">
    <form  class="form-group" action="update.php" method="post">

        <b>Edit Name:</b><br>
        <input class="form-control" type="text" name="name" value="<?php echo $oneData->name ?>">
        <br>
        <b>Choose Your Gender:</b><br><br>

        <input type="radio" name="gender" value="male" <?php if($oneData->gender=="male"):?>checked<?php endif ?> > Male<br>

        <input type="radio" name="gender" value="female" <?php if($oneData->gender=="female"):?>checked<?php endif ?>> Female<br><br>

        <input type="hidden" name="id" value="<?php echo $oneData->id ?>">

        <button type="submit" class="btn btn-success">Submit</button>

    </form>

</div>

</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


