<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;


  if(!isset($_SESSION)){
      session_start();
  }
  $msg = Message::getMessage();

  echo "<div id='message'> $msg </div>";



$objHobbies= new \App\Hobbies\Hobbies();
$objHobbies->setData($_GET);
$oneData = $objHobbies->view();
$array=explode(",",$oneData->hobby);

//$array=explode(",", "".$oneData['hobby']."");

//$array=explode(",",$oneData['hobby']);




?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies Edit Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

<style>
    .center_div{
        margin: 0 auto;
        width:80% /* value of your choice which suits your alignment */
    }
    </style>

</head>
<body>
<br><br>

<div class="panel container center_div">
    <div class="panel-heading" style="background-color:#1b6d85;color: #ffffff">
<h1 class="display-1 text-center">Edit Information</h1>
        </div>


<div class="container panel-body center_div">

    <form action="update.php" class="form-group" method="post">

        <b>Your Name:</b>
        <input type="text" class="form-control" name="name" value="<?php echo $oneData->name ?>" placeholder="Enter your name"><br>
        &nbsp;<b>Your Hobbies:</b>
        <div class="checkbox">
            &nbsp;&nbsp;<input type="checkbox" name="hobby[]" value="Gardening" <?php if(in_array("Gardening",$array)) : ?> checked <?php endif; ?> > Gardening</div>
        <div class="checkbox">
            &nbsp;&nbsp;<input type="checkbox" name="hobby[]" value="Reading books" <?php if(in_array("Reading books",$array)) : ?> checked <?php endif; ?> > Reading books</div>
        <div class="checkbox">
            &nbsp;&nbsp;<input type="checkbox" name="hobby[]" value="Music" <?php if(in_array("Music",$array)) : ?> checked <?php endif; ?>> Music</div>
        <div class="checkbox">
            &nbsp;&nbsp;<input type="checkbox" name="hobby[]" value="Facebooking" <?php if(in_array("Facebooking",$array)) : ?> checked <?php endif; ?>> Facebooking</div>
        <br><br>
        <input type="hidden" name="id" value="<?php echo $oneData->id?>" >

        <center><button type="submit" class="btn btn-success">Submit</button></center>

    </form>

</div>
    </div>

</center>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


