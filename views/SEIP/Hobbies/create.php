

<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js">

    </script>

    <style>
        .center_div{
            margin: 0 auto;
            width:80% /* value of your choice which suits your alignment */
        }
    </style>

</head>
<body>

<div class="container">

    <div class="navbar">

        <center><td><a href='index.php' class='btn btn-group-lg alert-success'>View Active-List</a>
            <a href="trashed.php"   class="btn btn-group-lg alert-warning role="button"> View Trashed List</a>
        </td></center>

    </div>
</div>
    <div class="panel container center_div">
        <div class="panel-heading" style="background-color:#67b168;color: #ffffff">
            <h1 class="display-1 text-center">Add Hobbies</h1>
        </div>
   <div class="panel-body">

    <form action="store.php" class="form-group" method="post">

        <b>Your Name:</b>
        <input type="text" class="form-control" name="name" placeholder="Enter your name"><br>
        &nbsp;<b>Your Hobbies:</b>
        <div class="checkbox">
            &nbsp;&nbsp;<input type="checkbox" name="hobby[]" value="Gardening"> Gardening</div>
        <div class="checkbox">
            &nbsp;&nbsp;<input type="checkbox" name="hobby[]" value="Reading books"> Reading books</div>
        <div class="checkbox">
            &nbsp;&nbsp;<input type="checkbox" name="hobby[]" value="Music"> Music</div>
        <div class="checkbox">
            &nbsp;&nbsp;<input type="checkbox" name="hobby[]" value="Facebooking"> Facebooking</div>
        <br><br>
        <center><button type="submit" class="btn bg-success btn-lg btn-block">Submit</button></center>

    </form>
    <br><br>
   </div>
</div>

<script src="../../../resource/bootstrap/js/jquery-1.11.1.min.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


